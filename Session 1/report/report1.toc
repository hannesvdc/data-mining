\select@language {english}
\contentsline {section}{\numberline {1}Function Approximation: Noiseless Case}{1}{section.1}
\contentsline {section}{\numberline {2}The role of the hidden layer and output layer}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}A linear regression model}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}A non-linear model}{2}{subsection.2.2}
\contentsline {section}{\numberline {3}Function Approximation: Noisy Case}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Impact of the number of data points}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Impact of the noise level}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Impact of the number of hidden neurons}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Impact of the training algorithm}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Early stopping versus iterating until convergence}{9}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Impact of the regularization parameter}{9}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Impact of the initial choice of weights}{13}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Conclusion on function approximation in the noisy case}{13}{subsection.3.8}
\contentsline {section}{\numberline {4}The Curse of Dimensionality}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Choice of Training, Validation and Test data}{13}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}One dimensional input space}{13}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Two dimensional input space}{14}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Five dimensional input space}{14}{subsection.4.4}
