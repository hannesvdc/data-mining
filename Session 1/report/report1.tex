\documentclass[a4paper,kul]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{float}
\usepackage{epstopdf}
\usepackage{rotating}
%\usepackage{gensymb}
\usepackage{booktabs}
\newcommand{\norm}[1]{\left\|{#1}\right\|}

\date{Academic year 2017-2018}
\address{
	Master Mathematical Engineering \\
	Data Mining and Neural Networks \\
	Prof. Dr. Ir. Suykens}
\title{Data Mining: Assignment 1}
\author{Hannes Vandecasteele}

\begin{document}
	\maketitle
	\tableofcontents
	
\section{Function Approximation: Noiseless Case}
In this section we will look at the relation between the complexity of a non-linear function to approximate and the number of hidden neurons needed for this. Let's first consider the cosine function with only one rising edge. Then intuitively one hidden neuron should be enough to approximate this function accurately since this hidden neuron uses a $\tanh$ activation function which looks similar to a cosine function on the rising edge. Indeed, Figure \ref{fig:d1h1} shows a good approximation and this was the case for all runs performed for this simple function. If we then in crease the complexity a bit to a cosine with three edges and keep one neuron as in Figure \ref{fig:d2h1}, then the neural net cannot fit the function well since it can only 'follow' one edge of the function. The points near the end of the interval are not approximated well at all since this is the saturation part of the $\tanh$ activation function. Hence intuitively we would need three neurons to fit well. This is shown in Figure \ref{fig:d2h3}. The algorithm converged to the correct cosine function almost always. but it sometimes happened that the optimization routine got stuck in a local minimum where the endpoints were not fitted correctly, as in figure \ref{fig:d2h3wrong}. Increasing the number of hidden neurons a little bit will alleviate this problem since there will be more degrees of freedom to fit the cosine correctly.

We can draw the same conclusion for a cosine function with seven rising or falling edges, then seven neurons is enough in principle to fit the function accurately as in Figure \ref{fig:d6h7}, as this also works nicely in about 90 percent of cases. Of course the optimization routine can again get stuck in a  local minimum as Figure \ref{fig:d6h7wrong}, so to increase the fit we could add one or two more neurons without much risk of over fitting. This phenomenon occurs when the number of hidden neurons is way to big for the complexity of the function as in Figure \ref{fig:d1h9}. There we have a simple cosine function with one rising edge but have chosen nine hidden neurons. The result is some weird overfitting between the sampling points of the cosine.

The conclusion is that we need at least as much hidden neurons as the number of rising and falling edges in the cosine function to approximate this function accurately. This makes sense since each neuron can then specialize in approximating one such flank since it will be mostly compensated by the other neurons in the other domains. Adding a few more neurons than the bare minimum is advised since sometimes the optimizer can get stuck in a local minimum and adding extra degrees of freedom can alleviate this problem. Also always be aware of overfitting but this only occurs if the number of hidden neurons is way bigger than the number of edges of the cosine function.
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=0.9\linewidth]{../plots/d1h1}
		\caption{Approximation of a cosine with one edge and one hidden neuron. This works nicely.}
		\label{fig:d1h1}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=0.9\linewidth]{../plots/d2h1}
		\caption{Fitting a cosine with three edges with only one neuron. The end points of the cosine are not approximated well at all.}
		\label{fig:d2h1}
	\end{subfigure}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=0.9\linewidth]{../plots/d2h3}
		\caption{Increasing the number of hidden neurons to three again gives a nice fit in most of the experiments.}
		\label{fig:d2h3}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=0.9\linewidth]{../plots/d2h3_wrong}
		\caption{Although it can still sometimes go wrong due to local minima.}
		\label{fig:d2h3wrong}
	\end{subfigure}
\end{figure}
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=0.9\linewidth]{../plots/d6h7}
		\caption{Approximation of a cosine function with seven edges works nicely with seven hidden neurons in most experiments.}
		\label{fig:d6h7}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=0.9\linewidth]{../plots/d6h7_wrong}
		\caption{But sometimes more degrees of freedom are needed to not get stuck in local minima.}
		\label{fig:d6h7wrong}
	\end{subfigure}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/d1h9}
	\caption{An example of overfitting with nine hidden neurons for a simple cosine function.}
	\label{fig:d1h9}
\end{figure}
\section{The role of the hidden layer and output layer}
\subsection{A linear regression model}
In a linear $n-$dimensional regression model the objective is to find weights ${w_j}$ and a bias term $\beta$ that minimize the least squares criterion
\[
\text{min} \sum_{p=1}^{P} (y_p-(w_{1}x_{p,1}+w_{2}x_{p,2}+\dots+w_{n}x_{p,n}))^2.
\]
Since the output function will be a linear function, a single neuron with the identity function as activation function will suffice since this will only propagate the sum $\sum_{i=1}^{n}w_ix_i$ to the output. Hence only one layer is necessary with only one neuron where all $n$ inputs are linked to.
\subsection{A non-linear model}
Consider a non-linear model function given by $y_p=-\cos(0.8\pi x_p)$ and plotted in Figure \ref{fig:cosine}. This function is almost linear in the middle and it curves a near the end of the interval. Hence the linear model from the previous section should work nicely with one neuron that scales the input with a weight and adds a bias term. Of course since the model will be a fit over the whole interval, it will perform worse near the edges where the cosine starts to curve more. Hence using a nonlinear activation function will do a slightly better job so we will choose two hidden neurons to approximate the cosine function, without much risk of overfitting.

Figure \ref{fig:activation1} shows a possible activation curve for both neurons as well as an approximation (in purple) of the cosine function in yellow. The original function is barely visible indicating a good fit. We see that one neuron (in blue) gives rise to a constant offset while the other function in orange gives the sinusoidal approximation of the cosine. The total approximation is a linear combination of both activation functions of the hidden neurons which is logical since the output layer only takes a weighted sum of its inputs. The output weights are indeed -0.85 for an offset (blue curve) and -1.16 to fit the overall curve. The output bias also was about 0.85 to almost annihilate the first activation function. The MSE for this experiment was about $1.1 \space 10^{-6}$ and finished after 15 iterations because the error in the validation data increased too much.

If we redo the experiment with another random initialisation of the weights in Figure \ref{fig:activation2} then the activation functions look a bit different but the total MSE is roughly the same $6.65 10^{-6}$ but the solver took more time to converge: 37 iterations. The fit looks visually about the same. In this plot, the blue activation function will be used to roughly follow the cosine pattern while the orange one will correct the blue one a little where necessary. We expect that the second activation will have a smaller weight. The output weights are indeed 0.85 and -0.25 approximately. The output bias is small with 0.05.

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{../plots/cosine}
	\caption{A sinusoidal input function for the neural network.}
	\label{fig:cosine}
\end{figure}
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\linewidth]{../plots/activation1}
		\caption{Two possible activation functions from the hidden neurons after training the network.}
		\label{fig:activation1}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\linewidth]{../plots/activation2}
		\caption{Two other possible activation functions with the same overall fit.}
		\label{fig:activation2}
	\end{subfigure}
\end{figure}

\section{Function Approximation: Noisy Case}
Let's now add Gaussian noise to the cosine function and see how the neural net behaves now. The input will always be a cosine with frequency 1 on the interval $[0,2\pi]$ hence it will have four rising and falling edges. The baseline algorithm will hence contain 5 hidden neurons and we will use the BFGS optimization algorithm unless speficied otherwise. The results will also always be averaged over 50 i.i.d. runs and the first five will consequently plotted in order to not drown the plot in lines. There will also be no regularization unless we are varying to investigate its effect.

\subsection{Impact of the number of data points}
Choose three different values for the number of data points: 30, 150 and 1200. The noise level was 0.2 for all these experiments. For $N=30$ the results are shown in Figure \ref{fig:N30}. The MSE is about 0.08 and on average the optimizer took 22 epochs to converge to a gradient of about 0.03. The overal fit is good although for some initial weights, the approximation does not look like the original cosine at all due to a local minimum.

If we increase the number of data points to $N = 150$ then the results are shown in Figure \ref{fig:N150}. The different approximations all lie closer to each other and the true cosine and this is also visible in the MSE: it decreased to 0.05. The number of epoch of course increased since there is more data to take into account. So increasing the amount of data increases the total fit here.

If we then again increase the number of data points to $N=1200$ in Figure \ref{fig:N1200} the MSE decreases a little bit to 0.049 but this is not significantly less than the 0.05 for $N=150$. Moreover the number of epochs (and thus computation time) increased to about 40. There is also no visible difference between the approximations for $N=150$ and $N=1200$. We can conclude that using more data can increase the fit significantly, as for $N$ from 30 to 150 but adding too much data only causes the error (MSE) to stagnate and it doesn't decrease accordingly. For the rest of this section we will hence use $N=150$ data points.
\begin{figure}
\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/N30}
	\caption{Different approximations for $N=30$.}
	\label{fig:N30}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/N150}
\caption{Different approximations for $N=150$.}
\label{fig:N150}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.2\linewidth]{../plots/N1200}
\caption{Different approximations for $N=1200$.}
\label{fig:N1200}
\end{subfigure}
\end{figure}

\subsection{Impact of the noise level}
We will now keep the number of data points constant to $N=150$ but vary the standard deviation of the gaussian additive noise between 0.2, 0.6 and 1.2. For noise level 0.2 the resulting approximations are shown in Figure \ref{fig:noise02}. This experiment was extactly the same as for Figure \ref{fig:N150} but with a different random initialisation of the weights. The approximations all seem reasonably well apart from one but due to an unlucky initial choise of weights. The MSE in this experiments was  0.06 about the same value as for Figure \ref{fig:noise02}.

Increasing the standard deviation of the noise to 0.6 gives rise in Figure \ref{fig:noise06}. The data points are more scattered and there are larger differences between the true function and its approximation. The MSE increased to 0.4 with is an order larger than in the previous experiments which is a logical result since it is more difficult to lie close to all points since they themselves are further apart from each other. The number of epochs of the optimizer remained about the same.

Then finally if we again increase the noise level to a standard deviation of 1.2, then the points and approximations are even more scattered in Figure \ref{fig:noise12}, especially near the boundaries of the interval. The reasoning is the same as before and the MSE increased to 1.83 which is yet another order of magnitude bigger than the previous experiment.

We can conclude that the noise level has a big impact on the total fit and MSE. This of course makes a lot of sense since the data points are on average a lot further apart and a function can never be close to all data points. The number of epochs however remained almost the same. The noise does not seem to have an impact on this, the number of data points does. The noise of course is a parameter that is very difficult or even impossible to control in practice. Therefore we will continue with a noise level of 0.2 in the rest of this report.
\begin{figure}
\centering
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/noise02}
\caption{Five approximations for $N=150$ and noise standard deviaton 0.2.}
\label{fig:noise02}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/noise06}
\caption{Five approximations for $N=150$ and noise standard deviaton 0.6.}
\label{fig:noise06}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/noise12}
\caption{Five approximations for $N=150$ and noise standard deviaton 1.2.}
\label{fig:noise12}
\end{subfigure}
\end{figure}

\subsection{Impact of the number of hidden neurons}
We already know from section 1 that the number of hidden neurons should at least be around the number of edges of the cosine (number of linearly independent parts of the function) in order to approximate it accurately. We can see the same effect here if we decrease the number of hidden neurons to two in Figure \ref{fig:hidden2}. The approximations only have one rising and falling part so they can never approximate the cosine well. There are in other words not enough degrees of freedom (weights) in the output layer. The MSE, as far it has any meaning in this case, is 0.45 which is a lot bigger than the results encountered so far for a noise level of 0.2. For 5 hidden neurons we already know that the approximations follow the true function well in Figure \ref{fig:N150}.

If we however use too many hidden neurons, like 10, in Figure \ref{fig:hidden10}, the approximations start to overfit the data points. The approximations seem to reach many small peeks, which are not in the true cosine function. The MSE is logically low with 0.05 since we are trying to lie as close as possible to the data points. Hence we are overfitting the data points and not the true function. We already saw this effect in section 1. The conclusion again is that using more hidden neurons will decrease the total error, but it will then also decrease this error artifically on training data if we add too many neurons without actually decreasing the error on test data. We will keep using five hidden neurons for the rest of this section.
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1.1\linewidth]{../plots/hidden2}
		\caption{Approximations with only 2 hidden neurons.}
		\label{fig:hidden2}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1.1\linewidth]{../plots/hidden10}
		\caption{Overfitting phenomenon with 10 hidden neurons.}
		\label{fig:hidden10}
	\end{subfigure}
\end{figure}

\subsection{Impact of the training algorithm}
In previous section we always used the BFGS-algorithm to compute the weights of the feedforward network. Other algorithms such as Levenberg-Marquardt, Conjugate Gradients and the standard backprogapation method are possible too. In this series of experiments we will be using $N=150$ points with a noise level of 0.2 and 5 hidden neurons. For completeness the typical result for a quasi-Newton procedure is shown in Figure \ref{fig:quasi-newton}. The average number of iterations was about 35, which is relatively big since many iterations are needed to converge to a solution but one iteration is very fast because the approximation of the Hessian matrix is updated iteratively and no exact computation of the Hessian is done.

For comparison, the Levenberg-Marquardt solution is shown in Figure \ref{fig:levenberg}. The number of iterations was a lot smaller: 20 on average. This is the case since now exact Hessians (up to a damping constant for robustness) are used and they are more intense to compute but the overall optimization routine will converge faster. The MSE of both methods was approximately equal to a value of 0.06, which is a very good result on the test set.

Completely different results arise when using the standard gradient-descent method in the back propagation algorithm. This is a Hessian-free method and is expected to converge slower. The approximations are in Figure \ref{fig:backpropagation}. The results are terrible. Most of the time the optimizer did not converge and stopped due to the maximum number of iterations were reached. The iterations themselves however were very fast since no hessian is necessary in this method. The bad fit is also expressed in the MSE: 0.33.

Finally we can also consider the conjugate gradient algorithm as a forth method. This method arises naturally when optimizing second degree polynomials in high dimensions (were it is analytically exact) but can also be applied to non-convex problems such as finding optimal weights in neural nets. Figure \ref{fig:conjugategradients} shows the approximations in this case. The number of iterations is on average somewhere in between Levenberg-Marquardt and quasi-Newton and it produces the same MSE. However the final gradient when the method stops due to a validation error in increase is on average at least one order bigger than the other two methods ($10^{-1}$ versus $10^{-2, -3}$). This indicates that the method has not yet fully converged before it is stopped hence the other two algorithms might be better.

The conclusion is that the optimization routine behind the neural net does not have much impact on the actual result and error. Levenberg-Marquardt and quasi-Newton methods seemed the best from the discussion above but conjugate gradients is also useful depending on the exact problem. Back propagation should however be avoided since it converges very slowly and needs many iterations.

\begin{figure}
\centering
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/quasinewton}
\caption{Approximations with $N=150$ and noise level 0.2 with the BFGS algorithm.}
\label{fig:quasinewton}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/levenberg}
	\caption{Approximations using the Levenberg-Marquardt method.}
	\label{fig:levenberg}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/backpropagation}
	\caption{Very bad fits using gradient descent.}
	\label{fig:backpropagation}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/conjugategradients}
	\caption{Neural net outputs using the conjugate gradient algorithm.}
	\label{fig:conjugategradients}
\end{subfigure}
\end{figure}

\subsection{Early stopping versus iterating until convergence}
In all the sections above we let the training algorithm stop when the error of the validation data did not converge anymore. This is necessary to avoid overfitting the neural net on the training data and to make it a useful model for different input data as well. The results are different when early stopping is not used and we let the neural net converge untill the error on the training set cannot be improved further. Figure \ref{fig:earlystopping} shows the results with early stopping and Figure \ref{fig:convergence} without. We can already see that without early stopping it can be the case that we overfit the training data as in the latter figure.  The MSE on the test data without early fitting also isn't significantly better (0.0485 with versus 0.048 without early stopping) as expected.
\begin{figure}
\centering
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/earlystopping}
\caption{Standard neural-net approximation with early stopping.}
\label{fig:earlystopping}
\end{subfigure}%
\begin{subfigure}[b]{0.5\linewidth}
\includegraphics[width=\linewidth]{../plots/convergence}
\caption{Approximation without early stopping. Some overfitting is visibile near the border for the purple approximation.}
\label{fig:convergence}
\end{subfigure}
\end{figure}
It also happens a lot that the optimization routine keeps on iterating without decreasing the gradient on the training data significantly. This is an indication that we have reached a local minimum and that the optimizer should stop. Early stopping will force the optimization to stop in that case. For example, in Figure \ref{fig:convergenceperformance} the optimizer converges after approximately 160 epochs because the gradient reached a certain threshold around $10^{-7}$. With early stopping, the algorithm would have stopped already when the MSE would have started increasing again on the validation data as in Figure \ref{fig:earlystoppingperformance}. The gradient in that case was however a lot bigger ( $10^{-2}$) but the MSE roughly the same and this is more important.

\begin{figure}
\centering
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/convergenceperformance}
\caption{The optimizer keeps iterating even though the MSE doesn't decrease anymore but the gradient does.}
\label{fig:convergenceperformance}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/earlystoppingperformance}
\caption{The MSE on the validation data increases and makes the optimizer stop while reaching almost the minimal MSE on the test set.}
\label{fig:earlystoppingperformance}
\end{subfigure}

The conclusion is twofold. First of all early stopping helps avoiding overfitting the training data by stopping the optimizer when the MSE on the validation data starts to increase. On the other hand it also makes it more efficient by stopping the optimizer does not improve the MSE anymore while the gradient may still be decreasing. The conclusion is hence to always use early stopping!
\end{figure}

\subsection{Impact of the regularization parameter}
Up till now, we didn't use a regularization parameter during training. Introducing such a parameter can improve the conditioning of the problem when there are a lot (millions) of degrees of freedom. The optimization problem is usually ill-conditioned in such case. Here we will consider regularization paramets 0, 0.1 and 0.5. In \texttt{MATLAB} this parameter is scaled between 0 and 1: 0 means no regularization while 1 will result in all weights and biases being zero. We used the samen dataset for all three different parameter values to make a more fair comparison. The results for 0 regularization is shown in Figure \ref{fig:reg0}, for 0.1 in Figure \ref{fig:reg01} and for 0.5 in Figure \ref{fig:05}. 

The effects are clear. Without regularization we get a nice fit with an MSE of about 0.06 which is low in  this context. If we increase the parameter to 0.1, the approximations deviate more from the true function, they are smaller in amplitude. This is a logical consequence since regularization will make the weights and bias terms smaller in absolute value. This effect is also visible in the MSE: it increased to 0.37. The number of epoch used also increased on average from about 25 to 45. Finally for 0.5, the approximations seem almost straight lines and there is no fit at all. The MSE is very bad around 0.4.

The conclusion is that regularization is absolutely necessary when there are many degrees of freedom in the optimization problem and can be used to suppress certain weights that are less important. If we choose this parameter value too high on the other hand, important weights and biases will also be decreased artificially, thereby decreasing the overall fit.

\begin{figure}
\centering
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/reg0}
\caption{Simulations with a regularization parameter of 0.}
\label{fig:reg0}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/reg01}
\caption{Regularization parameter increased to 0.1.}
\label{fig:reg01}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1.1\linewidth]{../plots/reg05}
\caption{All weights are completely suppressed with regularization parameter 0.5.}
\label{fig:reg05}
\end{subfigure}
\end{figure}

\subsection{Impact of the initial choice of weights}
We have already seen in all plots above that chosing a different set of initial weights can have a big impact on the total fit. For example in Figure \ref{fig:N30} the difference between the yellow line and all other approximations is purely the effect of the initial weights because the other training parameters are the same. We will not go into furhter detail here because the effect has become clear from the experiments above.

\subsection{Conclusion on function approximation in the noisy case}
We have examined the impact of many different training parameters when training a feedforward neural net. First we can conclude that adding more data points is usefull and will increase the fit as long as were are not adding to many. This is also related to the noise level. When the noise level is high, the fit will of course be worse but then it may also not be that meaningful anymore to increase the amount of data significantly. The MSE will not improve anymore from a certain amount.

Also the number of hidden neurons is extremely important. A bare minimum is necessary to be able to make a good fit (in this case this was around 3) but adding too many will only cause overfitting - an effect that we have seen multiple times by now. This is again related to the noise level. If the noise is high, adding extra neurons will not increase the fit significantly anymore.

The actual training algorithm used did not seem very important except for the case of backpropagation. This algorithm almost never converged and only stopped because the maximum number of iterations was reached and the fit was a lot worse than all other algorithms considered. Finally adding a regularization term also has a big effect on the result. If this parameter is too high, then all weights and biases will be suppressed artificially and will result in a bad fit. A smaller regularization value can be useful to improve the conditioning of the optimization problem if this forms a problem.

\section{The Curse of Dimensionality}
We will now investigate how the dimensionality of the function we are trying to approximate influences the number of hidden neurons and the overall performance. For this consider the toy function
\[
f(x) = \text{sinc}(\norm{x})
\] 
where sinc is the standard sinc implementation in Matlab. We will consider a one, two and five dimensional input space. The following sections will discuss the choice of training, validation and test data as well as the results for the different dimensions considered.

\subsection{Choice of Training, Validation and Test data}
A sinc function has an infinite number of local minima and maxima, so we cannot expect to fit the function accurately with a neural net far away from training points since the activation functions only have one rising or falling side so we cannot capture the oscillating behavior in points that are not close the training points. Figure \ref{fig:badtestdata} shows the neural net approximation in the one dimensional case with fifty equidistant points in the interval $[-4,4]$ as training data and fifty equidistant points in the interval $[-5,5]$ as test data. The results are horrible and show that even extrapolating slightly does yield very bad results for the sinc function.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/badtestdata}
	\caption{Extrapolation of the fitted sinc function yields very bad results.}
	\label{fig:badtestdata}
\end{figure}
In this section we will therefore always use training data in the interval $[-4,4]$ or its higher dimensional equivalents to capture a few local minima and maxima and validation and test data in the same interval to stay away from large extrapolation errors. The training algorithm will also always be the Levenberg-Marquardt method.

\subsection{One dimensional input space}
A sinc function on the interval $[-4,4]$ has seven local minima or maxima so we would expect that six or seven hidden neurons would model the function very well. To test this, Figures \ref{fig:m1h5}, \ref{fig:m1h6}, \ref{fig:m1h7} and \ref{fig:m1h8} show five different neural net approximations for five, six, seven and eight hidden neurons respectively. The training set consisted of fifty equidistant points in $[-4,4]$, the validation set of fifty equidistant points in $[-3.9, 3.9]$ and the test set of fifty equidistant points in $[-3.95,3.95]$.
The results are clear. Five hidden neurons doesn't always yield good results as the MSE is rather high with 0.168. For six hidden neurons this decreases drastically to 0.0041 and is about similar to seven hidden neurons. Eight hidden neurons even reduces this further to $2.16 10^{-7}$. Also on the plots it is clear that five hidden neurons is not enough while from six onwards, the fits get a lot better and we can stop here. Only the effects on the border of the training interval remains a problem and there eight hidden neurons are necessary to yield a good neural net approximation.

\begin{figure}
\centering
	\begin{subfigure}[b]{0.5\linewidth}
	\includegraphics[width=1.1\linewidth]{../plots/m1h5}
	\caption{5 hidden neurons.}
	\label{fig:m1h5}
\end{subfigure}%
\begin{subfigure}[b]{0.5\linewidth}
	\centering
	\includegraphics[width=1.2\linewidth]{../plots/m1h6}
	\caption{6 hidden neurons.}
	\label{fig:m1h6}
\end{subfigure}
\begin{subfigure}[b]{0.5\linewidth}
	\includegraphics[width=1.1\linewidth]{../plots/m1h7}
	\caption{7 hidden neurons.}
	\label{fig:m1h7}
\end{subfigure}%
\begin{subfigure}[b]{0.5\linewidth}
	\centering
	\includegraphics[width=1.1\linewidth]{../plots/m1h8}
	\caption{8 hidden neurons.}
	\label{fig:m1h8}
\end{subfigure}
\end{figure}

\subsection{Two dimensional input space}
When going to higher dimensions it is logical that the number of hidden neurons would increase because there is a lot more dynamic behavior to catch. The hope is that these neurons scale linearly with the dimensionality. Figures \ref{fig:m2h20}, \ref{fig:m2h25}, \ref{fig:m2h30} and \ref{fig:m2h35} show the section along the x-axis of the two dimension approximations for 20, 25, 30 and 35 hidden neurons respectively. Clearly 20 neurons is not enough to yield an accurate approximation, but 25 is already a lot better and 30 gives a very good fit. This is also visible in the MSE's, the decreas from 0.17 to 0.04 to 0.011 and finally 0.004. So we can conclude that around 30 or 35 hidden neurons for the two dimensional case yiels satisfying approximations.
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\linewidth}
	\includegraphics[width=1.1\linewidth]{../plots/m2h20}
	\caption{20 Hidden neurons.}
	\label{fig:m2h20}
\end{subfigure}%
\begin{subfigure}[b]{0.5\linewidth}
	\centering
	\includegraphics[width=1.1\linewidth]{../plots/m2h25}
	\caption{25 Hidden neurons.}
	\label{fig:m2h25}
\end{subfigure}
\begin{subfigure}[b]{0.5\linewidth}
	\centering
	\includegraphics[width=1.1\linewidth]{../plots/m2h30}
	\caption{30 Hidden neurons.}
	\label{fig:m2h30}
\end{subfigure}%
\begin{subfigure}[b]{0.5\linewidth}
	\centering
	\includegraphics[width=1.1\linewidth]{../plots/m2h35}
	\caption{35 Hidden neurons.}
	\label{fig:m2h35}
\end{subfigure}
\end{figure}

\subsection{Five dimensional input space}
For a five dimensional sinc functions, a lot of practical problems arise. First of all it is not possible anymore to use a grid of 50 points in every direction since this would add up to $50^5 =312500000$ floating point numbers. This is approximately 2.5 gigabytes of RAM. Second training takes an extreem amount of time with so many inputs. To speed up this process, the Levenberg-Marquardt method is not the best choice since it requires the exact computation of the Hessian matrix. Hence the BFGS strategy is better but will take more iterations. In practice this seems to be a good choice. Also the number of grid points is reduced to 10 in every direction and only on the interval $[-2,2]$ to make the problem computationally feasible.

The results are very bad. For hardware reasons, only experiments up to 100 hidden neurons are shown since more neurons took too much time to compute. The approximations along the x-axis for 50 and 100 hidden neurons are shown in Figures \ref{fig:m5h50} and \ref{fig:m5h100}. None of these results even looks like the sinusoid function. The conclusion here is that as the dimensionality increases, the number of data points increases exponentially with the grid size so we need many more neurons to approximate this function in five dimensions accurately and capture these new degrees of freedom. The neurons don't scale linearly with the dimensionality of the problem, as we already saw a little bit when going from one to two input dimensions. This is the so-called curse of dimensionality. The fact that 100 neurons don't return any reasonable result is a numerical proof of this.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/m5h50}
	\caption{Cross section on the x-axis for 50 hidden neurons.}
	\label{fig:m5h50}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/m5h100}
	\caption{Cross section on the x-axis for 100 hidden neurons.}
	\label{fig:m5h100}
\end{subfigure}
\end{figure}

\end{document}