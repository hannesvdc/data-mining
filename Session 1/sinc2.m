function s = sinc2(x)
    s = [];
    for i = 1:1:length(x)
        s = [s,sinc(norm(x(:,i)))];
    end
end