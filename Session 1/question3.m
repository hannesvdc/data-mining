close all;

N = 150;
neurons = 5;
noise = 0.2;

%train_x = linspace(-1,1,N);
% train_y = cos(2*pi*train_x) + noise*randn(size(train_x));
% val_x = linspace(-0.9,0.9, N);
% val_y = cos(2*pi*val_x) + noise*randn(size(val_x));
% test_x = linspace(-0.95, 0.95, N);
% test_y = cos(2*pi*test_x)+noise*randn(size(test_x));
% x = [train_x, val_x, test_x];
% y = [train_y, val_y, test_y];

figure();
n=50;
nplot = 5;
legends = cell(nplot+2,1);
performance = 0.;

for i = 1:n
    net = fitnet(neurons, 'trainbfg');
    net.performParam.regularization = 0.5;
    net.divideFcn = 'divideind';
    net.divideParam = struct('trainInd', 1:N, ...
                             'valInd', (N+1):(2*N), ...
                             'testInd', (2*N+1):(3*N));
    [net, tr] = train(net, x, y);
    train_yhat = net(train_x);
    test_yhat = net(test_x);
    performance = performance + perform(net, test_y, test_yhat);
    
    if i <= nplot
        plot(train_x, train_yhat, '-', 'LineWidth', 3); hold on;
        legends{i} = strcat('Approximation ', num2str(i));
    end
    
    clear train_yhat;
    clear net;
end
    
plot(train_x, train_y, 'r*'); hold on;
plot(train_x, cos(2*pi*train_x), '-', 'LineWidth', 3); hold on;
legends{nplot+1} = 'Training set';
legends{nplot+2} = 'True function';
hold off;
xlabel('X', 'FontSize',24);
legend(legends);

performance = performance/n;
disp(performance);