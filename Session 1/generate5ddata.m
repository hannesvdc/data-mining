function y = generate5ddata(x)
    y = [];
    N = length(x);
    for i =1:N
        for j=1:N
            for k=1:N
                for l=1:N
                    for o=1:N
                        y = [y, [x(i); x(j); x(k); x(l); x(o)]];
                    end
                end
            end
        end
    end
end