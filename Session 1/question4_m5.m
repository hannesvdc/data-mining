close all;
clear all;

% Initialization
m = 1;
N = 10;
neurons = 100;

train_x = generate5ddata(linspace(-2,2,N));
train_y = sinc2(train_x);

val_x = generate5ddata(linspace(-1.9, 1.9, N));
val_y = sinc2(val_x);

test_x = generate5ddata(linspace(-1.95, 1.95, N));
test_y = sinc2(test_x);

x = [train_x, val_x, test_x];
y = [train_y, val_y, test_y];

n=5;
nplot = 5;
legends = cell(nplot+2,1);
performance = 0.;

for i = 1:n
    i
    % train the network
    net = fitnet(neurons, 'trainbfg');
    net.performParam.regularization = 0.;
    net.divideFcn = 'divideind';
    net.divideParam = struct('trainInd', 1:(N^5), ...
                             'valInd', (N^5+1):(2*N^5), ...
                             'testInd', (2*N^5+1):(3*N^5));
    [net, tr] = train(net, x, y);
    sinccut = [zeros(2,N); linspace(-2,2,N); zeros(2,N)];
    sinccuty = net(sinccut);
    train_yhat = net(train_x);
    test_yhat = net(test_x);
    performance = performance + perform(net, test_y, test_yhat);
    
    if i <= nplot
        plot(sinccut(3,:), sinccuty, '-', 'LineWidth', 3); hold on;
        legends{i} = strcat('Approximation ', num2str(i));
    end
    
    clear train_yhat;
    clear net;
end

plot(linspace(-2,2,N), sinc(linspace(-2,2,N)), 'r*'); hold on;
plot(linspace(-1.95, 1.95, N), sinc(linspace(-1.95, 1.95, N)), '-', 'LineWidth', 3); hold on;
legends{nplot+1} = 'Training set';
legends{nplot+2} = 'True function';
hold off;
xlabel('X', 'FontSize',24);
legend(legends);

disp(performance)
clear train_yhat;
%clear net;