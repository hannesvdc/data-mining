close all;
clear all;

% Initialization
m = 1;
N = 50;
neurons = 35;

train_x1 = linspace(-4,4,N);
train_x2 = linspace(-4,4,N);
[train_x1m, train_x2m] = meshgrid(train_x1, train_x2);
train_x = [reshape(train_x1m, [1,N*N]); reshape(train_x2m, [1, N*N])];
train_y = sinc2(train_x);
surf(train_x1m, train_x2m, reshape(train_y, [N,N])); hold off;

val_x1 = linspace(-3.9,3.9,N);
val_x2 = linspace(-3.9,3.9,N);
[val_x1m, val_x2m] = meshgrid(val_x1, val_x2);
val_x = [reshape(val_x1m, [1,N*N]); reshape(val_x2m, [1, N*N])];
val_y = sinc2(val_x);

test_x1 = linspace(-3.95,3.95,N);
test_x2 = linspace(-3.95,3.95,N);
[test_x1m, test_x2m] = meshgrid(test_x1, test_x2);
test_x = [reshape(test_x1m, [1,N*N]); reshape(test_x2m, [1, N*N])];
test_y = sinc2(test_x);

x = [train_x, val_x, test_x];
y = [train_y, val_y, test_y];

n=50;
nplot = 5;
legends = cell(nplot+2,1);
performance = 0.;

for i = 1:n
    i
    % train the network
    net = fitnet(neurons, 'trainlm');
    net.performParam.regularization = 0.;
    net.divideFcn = 'divideind';
    net.divideParam = struct('trainInd', 1:(N*N), ...
                             'valInd', (N*N+1):(2*N*N), ...
                             'testInd', (2*N*N+1):(3*N*N));
    [net, tr] = train(net, x, y);
    sinccut = [linspace(-4,4,N); zeros(1,N)];
    sinccuty = net(sinccut);
    train_yhat = net(train_x);
    test_yhat = net(test_x);
    performance = performance + perform(net, test_y, test_yhat);
    
    if i <= nplot
        plot(sinccut(1,:), sinccuty, '-', 'LineWidth', 3); hold on;
        legends{i} = strcat('Approximation ', num2str(i));
    end
    
    clear train_yhat;
    clear net;
end

plot(train_x1, sinc(train_x1), 'r*'); hold on;
plot(test_x1, sinc(test_x1), '-', 'LineWidth', 3); hold on;
legends{nplot+1} = 'Training set';
legends{nplot+2} = 'True function';
hold off;
xlabel('X', 'FontSize',24);
legend(legends);

disp(performance)
clear train_yhat;
%clear net;