function y = sumsinc(x)
    s= 0.0;
    for i = 1:size(x)
        s = s+x(i)*x(i);
    end
    
    y = sinc(sqrt(s));
end