close all;

% Initialization
m = 1;
N = 50;
neurons = 7;

train_x = linspace(-4,4,N);
train_y = sinc(train_x);
val_x = linspace(-3.9, 3.9, N);
val_y = sinc(val_x);
test_x = linspace(-3.95, 3.95, N);
test_y = sinc(test_x);
x = [train_x, val_x, test_x];
y = [train_y, val_y, test_y];

n=50;
nplot = 5;
legends = cell(nplot+2,1);
performance = 0.;

for i = 1:n
    % train the network
    net = fitnet(neurons, 'trainlm');
    net.performParam.regularization = 0.;
    net.divideFcn = 'divideind';
    net.divideParam = struct('trainInd', 1:N, ...
                             'valInd', (N+1):(2*N), ...
                             'testInd', (2*N+1):(3*N));
    [net, tr] = train(net, x, y);
    train_yhat = net(train_x);
    test_yhat = net(test_x);
    performance = performance + perform(net, test_y, test_yhat);
    
    if i <= nplot
        plot(test_x, test_yhat, '-', 'LineWidth', 3); hold on;
        legends{i} = strcat('Approximation ', num2str(i));
    end
    
    clear train_yhat;
    clear net;
end

plot(train_x, train_y, 'r*'); hold on;
plot(test_x, sinc(test_x), '-', 'LineWidth', 3); hold on;
legends{nplot+1} = 'Training set';
legends{nplot+2} = 'True function';
hold off;
xlabel('X', 'FontSize',24);
legend(legends);

disp(performance)
clear train_yhat;
%clear net;