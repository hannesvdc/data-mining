close all;

% Part 2: input patterns
X = linspace(0,1,21);
Y = -cos(0.8*pi*X);
plot(X, Y, 'LineWidth', 3); % a linear model will not capture this information.
xlabel('X', 'FontSize', 24);
ylabel('Y', 'FontSize', 24);

% Part 4: train a neural net
net = fitnet(2);
net = configure(net, X, Y);
net.inputs{1}.processFcns = {};
net.outputs{2}.processFcns = {};
[net, tr] = train(net, X, Y);

% Part 5: obtain weights and activations
[biases, weights] = hidden_layer_weights(net);
hiddenfun = hidden_layer_transfer_function(net);
[outputbias, outputweights] = output_layer_weights(net);
outputfun = output_layer_transfer_function(net);

x1 = hiddenfun(X .* weights(1) + biases(1));
x2 = hiddenfun(X .* weights(2) + biases(2));
outputp = outputfun(x1*outputweights(1) + x2*outputweights(2) + outputbias);

figure();
plot(X, x1, 'LineWidth', 3); hold on;
plot(X, x2, 'LineWidth', 3); hold on;
plot(X, Y, 'LineWidth', 3); hold on;
plot(X, outputp, 'LineWidth', 3);
xlabel('X', 'FontSize', 24);
title('Activation functions', 'FontSize', 24);
legend({'x1p', 'x2p', 'yp', 'outputp'}, 'Fontsize', 24);