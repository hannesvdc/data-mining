%% Load the choles data
% This will create a 21x264 choInputs matrix of 264 patterns
% and a 3x264 matrix choTargets of output patterns.
load cho_dataset;
close all;

%% Standardize the variables
[pn, std_p] = mapstd(choInputs);
[tn, std_t] = mapstd(choTargets);

%% PCA
[pp, pca_p] = processpca(pn, 'maxfrac', 0.02);
[m, n] = size(pp);

%% Train a neural net using LM with the 21 inputs
Test_ix = 2:4:n;
Val_ix = 4:4:n;
Train_ix = [1:4:n, 3:4:n, 4:4:n];

mseavg = [0, 0, 0];
for i = 1:1:50
    net = fitnet(5, 'trainbr');
    net.divideFcn = 'divideind';
    net.divideParam = struct('trainInd', Train_ix, 'valInd', Val_ix, 'testInd', Test_ix);
    net = train(net, pp, tn);

    Yhat_train = net(pp(:, Train_ix));
    Yhat_test = net(pp(:, Test_ix));

    Ytest = choTargets(:,Test_ix);
    mses = (vecnorm((Ytest-Yhat_test)').^2);
    
    mseavg = mseavg + mses;
end
disp(mseavg/i);
