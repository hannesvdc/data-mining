% Generate the data set.
x = Xnorm;
t = Y;

% Set up network parameters.
nin = sum(ind==true);			% Number of inputs.
nhidden = 2;			% Number of hidden units.
nout = 1;			% Number of outputs.
aw1 = 0.01*ones(1, nin);	% First-layer ARD hyperparameters.
ab1 = 0.01;			% Hyperparameter for hidden unit biases.
aw2 = 0.01;			% Hyperparameter for second-layer weights.
ab2 = 0.01;			% Hyperparameter for output unit biases.
beta = 50.0;			% Coefficient of data error.

% Create and initialize network.
prior = mlpprior(nin, nhidden, nout, aw1, ab1, aw2, ab2);
net = mlp(nin, nhidden, nout, 'linear', prior, beta);

% Set up vector of options for the optimiser.
nouter = 2;			% Number of outer loops
ninner = 10;		        % Number of inner loops
options = zeros(1,18);		% Default options vector.
options(1) = 1;			% This provides display of error values.
options(2) = 1.0e-7;	% This ensures that convergence must occur
options(3) = 1.0e-7;
options(14) = 300;		% Number of training cycles in inner loop. 

% Train using scaled conjugate gradients, re-estimating alpha and beta.
for k = 1:nouter
  net = netopt(net, options, x(:,ind), t, 'scg');
  [net, gamma] = evidence(net, x(:,ind), t, ninner);
end

output = mlpfwd(net, Xnorm(:,ind));

classf = [];
for tresh = 0:0.1:1
    cp = output;
    cp(output>=tresh) = 1;
    cp(output<tresh) = -1;
    corrclas = sum(cp == Y);
    
    classf = [classf ; [tresh, corrclas]];
end