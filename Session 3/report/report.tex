\documentclass[kul,a4paper]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{float}
\usepackage[outdir=./]{epstopdf}
\usepackage{rotating}
%\usepackage{gensymb}
\usepackage{booktabs}
\newcommand{\norm}[1]{\left\|{#1}\right\|}

\date{Academic year 2017-2018}

\title{Data Mining: Assignment 3}
\author{Hannes Vandecasteele}

\begin{document}
	\maketitle
	\tableofcontents

\section{Dimensionality reduction by PCA analysis}
This section discusses how PCA can help reducing the input dimension of a neural network in order to train it faster and create a simpler network while only very slightly giving in to the error of the system. First we will train a neural net with one hidden layer with an input dimension of 21 and compare two different training algorithms by means on the MSE on each of the three different cholesterol types.  Afterwards we will reduce the number of inputs to the neural net and see how the results change. In all the performed experiments, the MSE's were averaged over 50 independent runs and the training-validation-test set was split up as 50\%-25\%-25\%.

\subsection{Number of hidden units}
The standard number of hidden units in the assignment was 5. Here we first investigate if this number is optimal or whether it can be improved by either adding or removing some neurons. Table \ref{tab:hiddenneurons} shows the MSE for the three different cholesterol components as a function of the number of hidden neurons, averaged over 50 independent runs.

\begin{table}[h]
	\centering
	\begin{tabular}{c|c|c|c}
		hidden & Type 1 & Type 2 & Type 3\\
		\hline
		3 & $ 0.4303 \ 10^6 $ & $1.1152 \ 10^6$ & $0.1111 \ 10^6$ \\
		4 & $ 0.4307 \ 10^6 $ & $1.1149 \ 10^6$ & $0.1110 \ 10^6$ \\
		5 & $ 0.4305 \ 10^6 $ & $1.1149 \ 10^6$ & $0.1110 \ 10^6$ \\
		10& $ 0.4305 \ 10^6 $ & $1.1145 \ 10^6$ & $0.1107 \ 10^6$ \\
	\end{tabular}
	\caption{MSE's for the three different types of cholesterol for neural nets with different hidden layer sizes. The results were averaged over 50 independent runs and trained with the Levenberg-Marquardt algorithm.}
	\label{tab:hiddenneurons}
\end{table}
From the table it is clear that adding a few more neurons or even removing a few does not contribute significantly to the total error so we will keep working with five hidden neurons as this trains very quickly with an average of only 15 iterations.

\subsection{Comparison with Bayesian Regularization}
Bayesian regularization worked well for the Wisconsin breast cancer classification problem so it is worth trying it here for this regression problem too. On advantage of Bayesian Regularization is that no validation set is needed to do early stopping because regularization itself can be used to avoid overfitting. This way the training data can also be extended with the validation data from Levenberg-Marquardt. The comparison with Levenberg-Marquardt for five hidden neurons is enumerated in Table \ref{tab:br}. 

\begin{table}[h]
	\centering
	\begin{tabular}{c|c|c|c}
		Training Method & Type 1 & Type 2 & Type 3\\
		\hline
		Levenberg-Marquardt & $ 0.4305 \ 10^6 $ & $1.1149 \ 10^6$ & $0.1110 \ 10^6$ \\
		Bayesian Regularization & $ 0.4306 \ 10^6 $ & $1.1147 \ 10^6$ & $0.1109 \ 10^6$ \\
	\end{tabular}
	\caption{MSE's for the three different types of cholesterol for neural nets trained with the Levenberg-Marquardt and Bayesian Regularization method.}
	\label{tab:br}
\end{table}

The results for both methods are the same indicating that using a different training algorithm does not improve the fitting results at all. Even for 10 hidden neurons and Bayesian regularization the mse's are equal so the minimal error for one hidden layer seems to be achieved approximately.

\subsection{Dimensionality Reduction using PCA}
The previous neural nets were trained taking all 21 different inputs into account. Not all of these inputs will as relevant to the cholesterol level of a person and some won't even be relevant at all. One way of identifying the inputs that really matter is by the so-called principle component analysis. This computes the 'directions of largest variance' of the data and throws away orthogonal directions that have only a very small relevance (variance) on the data. The assumption made is that only a few principles components can describe the data accurately. The choice made in this chapter is to throw away all orthogonal directions in the data that contribute to less than 0.01\% of the total variance in the data. Table \ref{tab:pca} compares the Bayesian Regularization method with 21 and 4 inputs.

\begin{table}[h]
	\centering
	\begin{tabular}{c|c|c|c}
		Training Method & Type 1 & Type 2 & Type 3\\
		\hline
		21 Inputs & $ 0.4306 \ 10^6 $ & $1.1147 \ 10^6$ & $0.1109 \ 10^6$ \\
		4 Inputs & $ 0.4310 \ 10^6 $ & $1.11539\ 10^6$ & $0.1112 \ 10^6$ \\
		1 Inputs & $ 0.4337 \ 10^6 $ & $1.1156 \ 10^6$ & $0.1110 \ 10^6$ \\
	\end{tabular}
	\caption{MSE's for the three different types of cholesterol for neural nets trained with the Bayesian Regularization method and number of inputs indicated in the left column.}
	\label{tab:pca}
\end{table}
There is again no difference between the MSE's for a different dimensionality of the input using a Bayesian training method. One important difference however is that the total training time for 50 independent experiments decreased from 90 seconds for 21 inputs to 25 seconds for 4 inputs. This is more than a factor of three. For much larger networks with possibly more hidden layers and more inputs, such a dimensionality reduction could cause a disproportional decrease in training time with practically no increase on error on the test set to there is no reason to use 21 inputs in this case when apparently 4 are enough to describe the outputs as accurate as possible. The network even becomes a lot simpler and this may be easier for doctors for example to describe the results to patients and to suggest improving some habits to conquer their high cholesterol levels.

The results for one and two inputs are also included in the table and based on the MSE, only in input seems to describe the data almost as accurate as four inputs do. The one input corresponds to more than 95\% of the total variation so the results are not that surprising in this case. However 95\% is not good enough in a medical context so here it is a good idea to take more inputs into account to decrease the neglected part of the variation even more.

\section{Automatic Relevance Determination}
Another way to reduce the dimensionality of the input space besides PCA is so-called ARD or Automatic Relevance Determination. The idea here is to associate a hyperparameter $\alpha_i$ to each input and update this after each training step until these values converge. If some of these alphas turn out to be small compared to the others than we can safely neglect this input as it has a very small or no effect at all on the output. We will test this here with two MATLAB demos: \texttt{demard} and \texttt{demev1} as well as for a given dataset.

\subsection{ARD on a demo: \texttt{demard}}
The MATLAB demo \texttt{demard} trains a neural network for the function $t = \sin(2 \pi x_1)$ where $x_1$ is sampled from a uniform distribution on the interval $x_1$ with some Gaussian noise added. Then there are also two more inputs to the neural network: $x_2$ is exactly the same as $x_1$ but with Gaussian noise with a higher standard deviation and finally $x_3$ is just sampled from a normal distribution. Intuitively we would expect that the first input has the highest relevance since the true output is only a function of the first input but due to the added noise the second input may also have some relevance. Finally the third input is completely unrelated to the output so it should have no relevance at all.

\vspace{2mm}
The demo uses a neural net trained with the scaled conjugate gradient optimizer and re-estimates the hyperparameters after each complete cycle through the data. It connects a weight to each neuron but we are only interested in the $\alpha$'s associated with the input neurons. After two training cycles the $\alpha$'s associated to each input are 0.183, 62.54465 and 110509. Due to the MATLAB implementation, the higher the associated value, the less relevance the input has. Here we conclude indeed that the first input has the highest relevance, the second one a bit less but not zero while the third input has no influence at all. In we look at the weights of the edges leaving the input nodes in Table \ref{tab:weights} we notice a similar thing. The first input has high weights, the second one intermediate and the third input has practically zero weights. The only reason that these are nonzero is because the dataset is finite so there will always be some numerical influence on the output.

\begin{table}[h]
	\centering
	\begin{tabular}{c|c|c}
		Input & weight 1 & weight 2 \\
		\hline
		 1 &   -3.11093   &  1.09616 \\
		2 & -0.13230   &  0.01344 \\
		3 & 0.00200  &  -0.00046 \\
	\end{tabular}
	\caption{The weights of the edges going from the associated input to each of the hidden neurons for the \texttt{demard} example in MATLAB.}
	\label{tab:weights}
\end{table}
Finally Figure \ref{fig:demard} shows the true and approximated function by the final neural network and this looks very similar to the data so ARD did a good job in this example.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{demard}
	\caption{Plot of the real function and the neural net approximation for the \texttt{demard} example.}
	\label{fig:demard}
\end{figure}

\subsection{Hyperparameters in MacKay's framework}
Another way of estimating hyperparameters of a neural network is by the so-called Evidence Maximization (EM) algorithm or a variant called MacKay's algorithm. This is in full detail described in \cite{mackay}. The Idea is to update the hyperparameters $\boldsymbol{\alpha}$ using a Bayesian approach. The main formula here is
\[
p(\boldsymbol{\alpha}|D) = \frac{p(D|\boldsymbol{\alpha}) p(\boldsymbol{\alpha})}{p(D)}
\]
where $p(\boldsymbol{\alpha}|D) $ is the posterior distribution given the parameters, $p(D|\boldsymbol{\alpha}) $ the likelihood of the data given the parameters and $p(\boldsymbol{\alpha})$ is the prior (unknown) distribution of the hyperparameters. The denominator is just a normalization constant to keep the probabilities between zero and one. In the example \texttt{demev1} sinusoidal data is given with Gaussian noise added. The inverse variation is 100. Mackay's algorithm tries to estimate a good fit to the data and the hyperparameters by first fixing the hyperparameters and then optimizing the neural net, after which the hyperparameters are updated again and so on until convergence.

The algorithm assumes a Gaussian prior distribution with inverse variance $\alpha$ and a likelihood with inverse variance $\beta$. The update step happens by minimizing a quadratic approximation of the logarithm of the posterior distribution and then training of the neural networks happens again. After three iterations the hyperparameters apparently are $\beta = 66.987$ and $\alpha=0.18016$. The inverse variance of the likelihood function is not quite $66.987$ but $100$ in reality so there seems to be a bias on the variance but this may also be due to the small and concentrated dataset.

It is also possible to draw confidence intervals around the neural network output due to the inherent uncertainty and distribution of the hyperparameters. These error bars correspond to one standard deviaten of the 'predictive distribution' of the neural net output. This is shown in Figure \ref{fig:demev1}. The confidence seems to be high (small interval) where there is actual measured data and it decreases uniformly as we go further away from data points. For inputs higher than 0.5 then true function deviates a lot from the neural net output and this is also resembled in the confidence intervals. These all seem very normal results.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{demev1}
	\caption{}
	\label{fig:demev1}
\end{figure}

\subsection{A realistic problem: Ionosphere data}
In practice it is also very important to reduce the dimensionality of a neural net to as few inputs as possible. Here we try out the ARD technique as in the \texttt{demard} example but on a dataset describing effects in the ionosphere. The network itself is a classifier with outputs between 1 and -1 with a linear output layer and there are 33 inputs and 351 input patterns in total. The number of hidden neurons is two, but as we have seen in a previous example this is good for a classifier and will give decent results here. A lot of the inputs won't be relevant at all on the output so the goal is to reduce the inputs to as few as possible.

We will look at the number of misclassifications on the complete dataset (training+test) as a measure of performance of the network. The number of good classifications for a network trained with all 33 inputs is shown in Table \ref{tab:full}. We let the threshold vary between 0 and 1 just to indicate that the results also depend on the choice of threshold but the results seem quite uniform as long as the threshold is not too high logically. The results were averaged over 50 independent runs and rounded to the nearest integer.

\begin{table}[h]
	\centering
	\begin{tabular}{c|c}
		Threshold & Good classifications \\
		\hline
		  0&  344 \\
		0.1&  344 \\
		0.2&  344 \\
		0.3&  344 \\
		0.4&  344 \\
		0.5&  348 \\
		0.6&  348 \\
		0.7&  346 \\
		0.8&  342 \\
		0.9&  330 \\
		1.0&  126 \\
	\end{tabular}
	\caption{The number of good classifications for the neural net trained with all 33 inputs and averaged over 50 independent runs.}
	\label{tab:full}
\end{table}

Let's now reduce the number of inputs. The exact values of the $\alpha$'s fluctuate a lot for each run but on average their order remains roughly the same. As for the \texttt{demard} example, we are interested in the smallest values for $\alpha$ since these correspond to highly relevant inputs. Table \ref{tab:alpha} contains the good classifications for a different number of relevant inputs as a function of the threshold.

\begin{table}[h]
	\centering
	\begin{tabular}{c|c|c|c|c|c}
		Threshold & 1 Input & 2 Inputs & 5 Inputs & 10 Inputs & 15 Inputs\\
		\hline
		0  &  263 & 278 & 290 & 323 & 339\\
		0.1&  263 & 273 & 288 & 333 & 340\\
		0.2&  263 & 271 & 274 & 333 & 340\\
		0.3&  263 & 272 & 274 & 333 & 340\\
		0.4&  263 & 264 & 270 & 333 & 339\\
		0.5&  126 & 264 & 268 & 330 & 339\\
		0.6&  126 & 164 & 263 & 330 & 339\\
		0.7&  126 & 126 & 250 & 323 & 332\\
		0.8&  126 & 126 & 239 & 319 & 331\\
		0.9&  126 & 126 & 197 & 126 & 325\\
		1.0&  126 & 126 & 126 & 126 & 126\\
	\end{tabular}
	\caption{The number of good classifications for the neural net trained with all 33 inputs and averaged over 50 independent runs.}
	\label{tab:alpha}
\end{table}
The results do not seem so good. For a very low number of inputs, 1 or 2, the classifier behaves very bad even for lower thresholds. We need to increase the inputs to 15 to have a decent approximation of the behaviour of the full system but this is only a reduction of a factor of two. This is of course application-specific. The ionosphere problem hence seems like a very complicated physics problem that depends significantly on many inputs, unfortunately for them.
\end{document}