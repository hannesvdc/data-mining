\select@language {english}
\contentsline {section}{\numberline {1}Dimensionality reduction by PCA analysis}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Number of hidden units}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Comparison with Bayesian Regularization}{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Dimensionality Reduction using PCA}{2}{subsection.1.3}
\contentsline {section}{\numberline {2}Automatic Relevance Determination}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}ARD on a demo: \texttt {demard}}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Hyperparameters in MacKay's framework}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}A realistic problem: Ionosphere data}{4}{subsection.2.3}
