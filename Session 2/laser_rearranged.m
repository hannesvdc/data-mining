close all;

autocor = xcorr(lasertrain);
[~,I] = sort(autocor, 'descend');
I = I(I > 1000);
I = I-1000;
plot(autocor);

% Plot the input data
figure();
plot(lasertrain); hold on;
plot(1001:1100, laserpred);

p = 20;
indices = I(1:p);
[traindata, traintarget]=getOrderedTimeSeriesTrainData(lasertrain, indices);

nplot = 5;
legends = cell(nplot+1,1);
mse_avg = 0.;
it = 50;
for j = 1:it
    disp(j);
    net = fitnet(2*p);
    net = configure(net, traindata, traintarget);
    [net, ~] = train(net, traindata, traintarget);

    % Now predict the error on the manual test set.
    predinputs = lasertrain;
    outputs = laserpred;
    netoutputs = [];
    for i=1:(length(laserpred))
       input = predinputs(end-indices+1);
       output = net(input);
       predinputs = [predinputs; output];

       netoutputs = [netoutputs; output];
    end

    % Compute mse
    mse_error = (outputs-netoutputs)'*(outputs-netoutputs)/length(outputs);
    mse_avg = mse_avg + mse_error;
    
    if j <= nplot
        figure(3);
        t = 1:1:length(outputs);
        plot(t, netoutputs, '-', 'LineWidth', 2); hold on;
        legends{j} = strcat('Approximation ', num2str(j));
    end
    
    clear net;
    clear netoutputs;
end

t = 1:1:length(outputs);
figure(3)
plot(t, outputs, '-', 'LineWidth', 2); hold on;
legends{nplot+1} = 'Laser Prediction Set';
hold off;
xlabel('Time [s]', 'FontSize', 24);
legend(legends);
disp(mse_avg/it);