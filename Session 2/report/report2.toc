\select@language {english}
\contentsline {section}{\numberline {1}Santa Fe laser data - time-series prediction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Choice of the optimal delay parameter}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Effect of the number of internal neurons}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Comparison of different training algorithms}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Impact of regularization}{6}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Rearranging inputs}{6}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Conclusion on the Santa Fe model}{6}{subsection.1.6}
\contentsline {section}{\numberline {2}Alfabet recognition}{9}{section.2}
\contentsline {subsection}{\numberline {2.1}Impact of the noise level for the second network}{9}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Impact of the number of hidden neurons}{9}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Conclusion}{10}{subsection.2.3}
\contentsline {section}{\numberline {3}Classification: Breast Cancer Wisconsin}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Optimal choice of hidden neurons}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Comparison of different training algorithms}{14}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Impact of regularization}{16}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Conclusion}{16}{subsection.3.4}
