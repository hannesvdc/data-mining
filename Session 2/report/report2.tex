\documentclass[kul,a4paper]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{float}
\usepackage[outdir=./]{epstopdf}
\usepackage{rotating}
%\usepackage{gensymb}
\usepackage{booktabs}
\newcommand{\norm}[1]{\left\|{#1}\right\|}

\date{Academic year 2017-2018}

\title{Data Mining: Assignment 2}
\author{Hannes Vandecasteele}

\begin{document}
	\maketitle
	\tableofcontents
	
\section{Santa Fe laser data - time-series prediction}
Consider the Santa Fe laser data set with 1000 training points where we use the standard division of 70\% training set, 15\% validation data and 15\% test data. The results are always averaged over 50 independent runs. We will first discuss several training methods and an optimal choice for the delay, and finally consider effect of rearranging the input order.

\subsection{Choice of the optimal delay parameter}
Denote the delay parameter by $p$. An interesting way to look at possible good values for $p$ is by plotting the autocorrelation function. High values in the autocorrelation for a specific value of the delay $p$ indicates that the signal is more similar to a shifted version over a distance $p$ with itself than for other delay values. The autocorrelation, together with a higher resolution version are plotted in Figures \ref{fig:autocorrelation} and \ref{fig:autocorrelationzoom}. We clearly see that for certain values of the delay $p = 15, 22, 30$ the autocorrelation reaches a maximum and it may be a good idea to use these as delay parameter.
\begin{figure}[h]
	\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/autocorrelation}
	\caption{Autocorrelation of the training data.}
	\label{fig:autocorrelation}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/autocorrelationzoom}
	\caption{Zoom of the autocorrelation to relevant small values of the delay.}
	\label{fig:autocorrelationzoom}
\end{subfigure}
\end{figure}

For the training, we use the standard Levenberg-Marquardt algorithm with early stopping without regularization. The number of hidden neurons scales with the delay and is $2p$. Figures \ref{fig:p15}, \ref{fig:p22} and \ref{fig:p30} show five different plots for the testdata for $p=15, 22$ and 30. The corresponding MSE's are $2.7 10^4$, $1.05 10^4$ and $3.9 10^4$.
\begin{figure}
	\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p15}
	\caption{Delay $p=15$.}
	\label{fig:p15}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p22}
	\caption{Delay $p=22$.}
	\label{fig:p22}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p30}
	\caption{Delay $p=30$.}
	\label{fig:p30}
\end{subfigure}
\end{figure}

All figures seem to follow the actual data nicely for time smaller than 50, but  $p=15$ seems the most satisfying. There even is a purple line that follows the exact data accurately when the amplitude decreases drastically. However on average $p=22$ seems to have a lower MSE although the difference is not much.  For $p=30$ all neural nets seem to have a way too big amplitude for the small regime and this is also mirrored in the MSE. We will continue with $p=22$ based on the MSE in further subsections.

\subsection{Effect of the number of internal neurons}
In the previous experiment, the number of internal neurons was kept constant to $2p$. Here we will see the effect of varying this parameter for $p=22$. Intuitively is seems logical that we would need at least the same number of hidden neurons as the delay of our system, however table \ref{tab:hiddenneurons} shows that this is not necessarily the case. The MSE's for a whole range of hidden layer sizes form $0.5p$ to $2p$ are about the same and it only seems to increase for $3p$. The plots can also be seen on Figures \ref{fig:p22h11} to \ref{fig:p22h66}.
\begin{table}
\centering
\begin{tabular}{c|c}
	hidden & MSE \\
	\hline
	11 & $ 1.8 10^4$ \\
	16 & $2.2 10^4$ \\
	22 & $1.24 10^4$\\
	27 & $1.3 10^4$ \\
	44 & $1.3 10^4$ \\
	66 & $4.6 10^4$
\end{tabular}
\caption{MSE's for different sizes of the hidden layer for $p=22$.}
\label{tab:hiddenneurons}
\end{table}
\begin{figure}
	\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p22h11}
	\caption{11 Hidden Neurons.}
	\label{fig:p22h11}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p22h16}
	\caption{16 Hidden Neurons}
	\label{fig:p22h16}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p22h22}
	\caption{22 Hidden Neurons}
	\label{fig:p22h22}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p22h27}
	\caption{27 Hidden Neurons.}
	\label{fig:p22h27}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p22h44}
	\caption{44 Hidden Neurons.}
	\label{fig:p22h44}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/p22h66}
	\caption{66 Hidden Neurons.}
	\label{fig:p22h66}
\end{subfigure}
\end{figure}
Based on the MSE we cannot really make a definitive conclusion about the number of hidden neurons. They give about the same results and are not able to follow the regime swicting of the true time series accurately. Also based on the plots, no plot gives truely better visual results than the others. We will keep using 44 hidden neurons in the next subsection to allow enough degrees of freedom.

\subsection{Comparison of different training algorithms}
The previous sections always used the Levenberg-Marquardt (Figure \ref{fig:lm}) algorithm to train the neural network. Here we will additionally consider Bayesian Regularization (Figure \ref{fig:br}), Scaled Conjugate Gradients (Figure \ref{fig:scg}) and also the BFGS optimization routine (Figure \ref{fig:bfgs}). The mean-squared errors are listed in Table \ref{tab:trainingalgorithms}.


The errors are all of the same order so it is not really possible to make a clear distinction between the algorithms based on this feature alone. The Bayesion method took very long to compute but gives visually the most appealing results. It is able to follow the exact laser data relative closely and the dark blue line can even predict the regime switch after 70 steps. The downside is that it took many (1000) iterations to compute. The BFGS method and SCG on the other hand do not seem to follow the exact solution visually after more than 30 steps so we will not consider these further. We can conclude that Bayesian Regularization is better at following the laser data visually eventhough this is not reflected in the MSE.
\begin{table}
	\centering
	\begin{tabular}{c|c}
		method & MSE \\
		\hline
		Scaled Conjugate Gradients & $3.2 10^4$ \\
		Levenberg-Marquardt & $1.21 10^4$ \\
		BFGS & $6.4 10^4$ \\
		Bayesian Regularization & $2.54 10^4$ \\
	\end{tabular}
	\caption{MSE's for different training algorithms.}
	\label{tab:trainingalgorithms}
\end{table}

\begin{figure}
	\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/bayes}
	\caption{Bayesian Regularization.}
	\label{fig:bayes}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}s
	\includegraphics[width=1.1\linewidth]{../plots/bfgs}
	\caption{BFGS.}
	\label{fig:bfgs}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/lm}
	\caption{Levenberg-Marquardt.}
	\label{fig:lm}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/scg}
	\caption{Scaled Conjugate Gradients.}
	\label{fig:scg}
\end{subfigure}
\end{figure}

\subsection{Impact of regularization}
Previous experiments all used a regularization parameter of zero. Making this value nonzero may reduce the number of degrees of freedom and possible result in better fits. We will test this for the Levenberg-Marquardt method and different regularization parameters listed in table \ref{tab:regularization}. Figures \ref{fig:mu7} to \ref{fig:mu1} also show the plots for this range of parameters. From the table, no clear trend is visible in the MSE's when increasing or decreasing the value of the regularization parameter. Visually they actually become worse so there is no real reason to include such a parameter in our neural network model of the laser data.

\begin{table}
	\centering
	\begin{tabular}{c|c}
		Regularization parameter & MSE \\
		\hline
		$10^{-7}$ & $1.6 10^4$ \\
		$10^{-5}$ & $2 10^4$ \\
		$10^{-3}$ & $1.3 10^4$ \\
		$10^{-1}$ & $8.9 10^3$ \\
	\end{tabular}
	\caption{MSE's for different regularization parameters.}
	\label{tab:regularization}
\end{table}

\begin{figure}
	\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/mu7}
	\caption{Regularization parameter $10^{-7}$.}
	\label{fig:mu7}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/mu5}
	\caption{Regularization parameter $10^{-5}$.}
	\label{fig:mu5}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/mu3}
	\caption{Regularization parameter $10^{-3}$.}
	\label{fig:mu3}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/mu1}
	\caption{Regularization parameter $10^{-1}$.}
	\label{fig:mu1}
\end{subfigure}
\end{figure}

\subsection{Rearranging inputs}
So far we always used an input vector $[y_k, y_{k-1}, \dots, y_{k-p}]$ with a certain delay parameter $p$ to predict the next output of the model $y_{k+1}$. Not all inputs $y_{k-i}$ may not be that useful however. If we look back at the autocorrelation plot in Figure \ref{fig:autocorrelationzoom} than certain delays may have a much higher impact than others. Therefore only selectively using certain input delays may yield better fits. In this section our delay parameter $p$ will indicate how many peeks from Figure \ref{fig:autocorrelationzoom} we will take into account in our neural network model. We will of course only use positive delays since the model is causal and the autocorrelation is symmetric. Table \ref{tab:delays} shows the MSE's for different values for the number of peaks.


We see that at first the MSE decreases steadily for increasing delays and is even able to halve the best MSE we have had so far for 15 peeks of the autocorrelation taken into account. This is a decent result but if we look at the plot it is debatable whether the predictions follow the true data better than in previous cases.

We can conclude that selectively using inputs does yield a better fit but not as much as anticipated.
\begin{table}
	\centering
	\begin{tabular}{c|c}
		Number of peaks & MSE \\
		\hline
		2 & $8.6 10^5$ \\
		5 & $2 10^4$ \\
		10 & $6.6 10^3$ \\
		15 & $8.5 10^3$ \\
		20 & $1.0 10^4$ \\
	\end{tabular}
	\caption{MSE's for different number of peaks in the autocorrelation as inputs in the neural network.}
	\label{tab:delays}
\end{table}

\begin{figure}
	\centering
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/delay5}
	\caption{5 peaks.}
	\label{fig:delay5}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/delay10}
	\caption{10 peaks.}
	\label{fig:delay10}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/delay15}
	\caption{15 peaks.}
	\label{fig:delay15}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.1\linewidth]{../plots/delay20}
	\caption{20 peaks.}
	\label{fig:delay20}
\end{subfigure}
\end{figure}

\subsection{Conclusion on the Santa Fe model}
We have tried out several options to improve the predictions for the Santa Fe time series. First by modifying the delay parameter, then by choosing the optimal number of hidden neurons, also by considering different training methods, different regularization parameters and finally by selectively choosing our input parameters. The conclusion is that a delay of $p=22$ is fine with $2p$ hidden neurons and the model seemed quite resilient against varying this number of hidden neurons. The Bayesian Regularization algorithm seemed to work sightly better visually but took much longer to converge, while other training algorithms considered performed worse than the standard Levenberg-Marquardt method. Regularization did not improve the fit at all and finally rearranging the inputs yielded slightly better results but not as much as anticipated.

The main conclusion is that standard feedforward neural networks may not be the best way to model this inherent recursif data and that feedforward models considered here were not able to follow the regime switch of the true data accurately.



\section{Alfabet recognition}
The MATLAB demo tries to classify a very simple $5 \times 7$ bitmap as one of the 26 different characters of our alphabet. This is achieved with two different feedforward neural networks. The first network is trained on a number of exact replications of the characters in our alphabet while the second network is trained on the same characters but with noise added to them. Intuitively the first network will be less resilient to noisy images because it 'has not learned' how to deal with noisy images while the second network has. As an example consider a two layer network with 25 hidden neurons, where one network is trained with exact characters while the second one with noise characters with a noise between zero and one. The networks are then tested by feeding images with different noise levels to the classifier and the results are shown in Figure \ref{fig:standardalphabet}. Indeed the first network has a higher amount of misclassifications than the second as expected intuitively.
We can now play with the number of hidden neurons and the noise level used for training the second network and see if anything spectacular changes. In all the following experiments we will not average over multiple independent runs because it won't be necessary to prove a point.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/standardalphabet}
	\caption{Percentage of misclassifications as a function of the noise level for the two different networks.}
	\label{fig:standardalphabet}
\end{figure}

\subsection{Impact of the noise level for the second network}
Figure \ref{fig:noiselevel} contains the error plots where we trained the second network for several noise levels. This noise ranged from 0.1 to 1. The results are a bit surprising since the errors don't increase significantly when the noise is decreased a lot. Moreover, the error plots for the second network don't seem to converge to the error plot for the noiseless network when the noise level of the second network goes to zero. The conclusion that we are able to draw is that adding even a slight amount of noise to the training data can increase the performance of a classification network significantly since it learns how to deal with noise.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/noiselevel}
	\caption{Error plot of the second network where this network was trained with inputs with different noise levels.}
	\label{fig:noiselevel}
\end{figure}

\subsection{Impact of the number of hidden neurons}
Besides the training noise level for the second network, the number of hidden neurons may also have a significant impact on the behaviour of the classifiers. Figure \ref{fig:hidden1} shows the error plots for the first neural net for different hidden layer sizes and Figure \ref{fig:hidden2} for the second net. The results are quite clear. For the first net, the errors decrease steadily uniformly over all noise levels as the hidden layer size increases up to a size of about 25 hidden neurons. This seems to be an optimal number and is was also the standard amount in the MATLAB demo. The conclusion is quite similar for the second network although the errors do not improve any more after about 20 hidden neurons probably due to the noise. The fact that this network was trained with noise makes it a more resilient classifier as discussed above but apparently also allows it to have fewer hidden neurons than the first network. This is an asset since it requires fewer computations.

\subsection{Conclusion}
We can draw two strong conclusions based on this small experiment. First of all, adding noise to the training input can improve the classifier more than proportionally since it makes the classifier resilient against noise. Of course the noise power should not be more than the energy of the input signal but as explained above just a little bit of noise can already have big effects. Second, adding noise may also allow reducing the network with a few neurons since these extra neurons are not needed to 'handle noise' as in the noiseless input case. This should of course be investigated in a lot more detail with analytical results but it was visible in the experiment above.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/hidden1}
	\caption{Error plot of the first neural network as a function of the hidden layer size.}
	\label{fig:hidden1}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/hidden2}
	\caption{Error plot of the second neural network as a function of the hidden layer size.}
	\label{fig:hidden2}
\end{figure}

\section{Classification: Breast Cancer Wisconsin}
This section discusses a binary classification problem based on a dataset that describes the distribution of breast cancer in the American state Wisconsin. This dataset contains 569 patients where every record has a label indicating whether the person has breast cancer or not (1 vs. -1). In total 30 different numerical values are considered as input. We will discuss for several neural net parameters the ROC curve and the misclassifications averaged over 50 independent runs. The ROC curve is an important tool to compare classifiers where the False Positive Rate is plotted against the Sensitivity. These are defined as follows.

\begin{align}
\text{False Positive Rate} &= \frac{\text{FP}}{\text{FP}+\text{TN}} \\
\text{Sensitivity} &= \frac{\text{TP}}{\text{TP}+\text{FN}}
\end{align}
where TP is the number of correctly classified positive cases (True positives), TN the number of correctly classified negative cases (True negatives), FP the number of cases that are negative but classified as positive (False positives) and FN the number of cases classified negative while they are positive in reality (False negatives). We will first look at the number of hidden neurons necessary and then at different training algorithms.

\subsection{Optimal choice of hidden neurons}
As this is a binary classification problem we can expect that only one hidden neuron should be enough to classify cases correctly with a high probability. Indeed, as the hidden neuron has a non-linear increasing activation function (transig in the MATLAB patternnet function), we could use a certain threshold $T$ to classify cases as positive if the output is higher than $T$ and negative otherwise.

Figure \ref{fig:h1} shows five ROC curves for five independent runs of the neural net with one hidden unit and the Levenberg-Marquardt algorithm. The results are very good. The ROC-curve looks almost rectangular indicating very good behavior. The number of misclassifications in Table \ref{tab:misclassified} seems to reach a minimum around a threshold value of 0.1 but then still 2\% of cases on the whole dataset are not classified correctly. This is as such a very good result for a simple single neuron neural net classifier but is not satisfactory in medical applications. It may be a good idea to introduce more hidden neurons.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/h1}
	\caption{ROC curve for only one hidden unit.}
	\label{fig:h1}
\end{figure}

\begin{table}
	\centering
	\begin{tabular}{c|c|c|c}
		Threshold & 1 hidden neuron & 2 hidden neurons & 5 hidden neurons\\
		\hline
		0 & 3.04\% & 2.51\% & 2.19\% \\
		0.1 & 1.57\%& 1.75\% & 1.91\% \\
		0.2 & 1.82\% & 1.82\% & 3.2\% \\
		0.3 & 2.46\% & 2.05\% & 2.01\% \\
		0.4 & 2\% & 2.86\% & 2.44\% \\
		0.5 & 2.3\% & 2.52\% & 2.52\% \\
		0.6 & 2.8\% & 2.64\% & 3.76\% \\
		0.7 & 3.07\% & 3.35\% & 4.05\% \\
		0.8 & 7.12\% & 4.81\% & 5.04\% \\
		0.9 & 11.13\% & 7.3\% & 9.19\% \\
		1.0 & 37.26\% & 37.26\% & 37.26\% \\
	\end{tabular}
	\caption{Percentage of misclassified data points of the whole dataset as a function of the number of hidden neurons and the threshold.}
	\label{tab:misclassified}
\end{table}

\noindent
The ROC-curve for two hidden units is drawn in Figure \ref{fig:h2}. It is visually almost indistinguishable form the ROC curve for one hidden neuron. If we again look at the percentage of misclassifications for two hidden neurons and compare them with the previous column then there are no big improvements, except apparently for larger thresholds. The minimal misclassification error however remains around the same value as one hidden neuron.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/h2}
	\caption{5 independent ROC curves for two hidden neurons.}
	\label{fig:h2}
\end{figure}

\noindent
Finally we can also look if there are any improvements if we increase the number of hidden neurons more to five. This ROC-curve is shown in Figure \ref{fig:h5} and the misclassification percentages are also shown in Table \ref{tab:misclassified}. Five hidden neurons does not seem to improve anything compared to 2 hidden neurons so the main conclusion is that, as expected, one hidden neuron delivers very good results for this classification problem, specifically when the threshold is close to 0. This is logical since if the threshold were too high then more and more true positive cases could be classified as a negative and the same vice versa.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/h5}
	\caption{ROC-curve for five hidden neurons.}
	\label{fig:h5}
\end{figure}

\subsection{Comparison of different training algorithms}
Up till now we only used the Levenberg-Marquardt training algorithm and this gave decent results with one hidden neuron, but maybe different training algorithms could perform better. We will again compare with Bayesian regularization and Scaled Conjugate Gradients. The ROC-curve for Bayes is shown in Figure \ref{fig:rocbayes} and is visually again indistinguishable from the Levenberg-Marquardt method \ref{fig:h1}. Therefore we look at the number of misclassifications as a function of the threshold for different methods in Table \ref{tab:cancertrainingalgorithms}. The results of Bayesian regularization are spectacular as it misclassifies fewer cases on average for all values of the threshold! The percentage even seems quite uniform over the different threshold values. This means we can take high threshold values to discriminate breast cancer since we don't want people to worry whether they have breast cancer and only tell them if we are very confident (=high threshold). The computational training cost for Bayesian Regularization is a little bit higher however but is not a real problem since the neural net performs better. Finally for completeness the ROC-curve for the scaled conjugate gradients method is shown in Figure \ref{fig:rocscg}. This seems a very bad curve compared to all previous ones so we will not consider this method further.

\begin{table}
	\centering
	\begin{tabular}{c|c|c|c}
		Threshold & Levenberg-Marquardt & Bayesian Regularization & Scaled Conjugate Gradients \\
		\hline
		0 & 3.04\% & 0.83\% & 2.19\% \\
		0.1 & 1.57\%& 0.86\% & 1.91\% \\
		0.2 & 1.82\% & 0.72\% & 3.2\% \\
		0.3 & 2.46\% & 0.79\% & 2.01\% \\
		0.4 & 2\% & 0.77\% & 2.44\% \\
		0.5 & 2.3\% & 0.83\% & 2.52\% \\
		0.6 & 2.8\% & 0.76\% & 3.76\% \\
		0.7 & 3.07\% & 0.75\% & 4.05\% \\
		0.8 & 7.12\% & 0.81\% & 5.04\% \\
		0.9 & 11.13\% & 0.76\% & 9.19\% \\
		1.0 & 37.26\% & 37.26\% & 37.26\% \\
	\end{tabular}
	\caption{Percentage of misclassified data points of the whole dataset for different trainings methods and one hidden neuron.}
	\label{tab:cancertrainingalgorithms}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/rocbayes}
	\caption{The ROC-curve for Bayesian Regularization with one hidden neuron.}
	\label{fig:rocbayes}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../plots/rocscg}
	\caption{The ROC-curve for Bayesian Regularization with one hidden neuron. The results are very bad and there is a high variability between successive runs.}
	\label{fig:rocscg}
\end{figure}

\subsection{Impact of regularization}
The model so far considers thirty different inputs that may have an effect on whether a person has breast cancer or not. This is a quite large number of parameters so some of them can possibly have a very small impact. Regularization can help suppressing these parameters if necessary. This method is not applicable for Bayesian Regularization (what's in the name) but we could see if the Levenberg-Marquardt method with regularization could perform better. However the minimal weight in absolute value for a one-hidden-neuron model is only 0.034 so regularization probably won't have a positive impact. Indeed, Table \ref{tab:lmreg} shows the number of misclassifications with a very small regularization parameter of $10^{-7}$ and the results are very bad.


\begin{table}
	\centering
	\begin{tabular}{c|c|c}
		Threshold & 0 & $10^{-7}$  \\
		\hline
		0 & 3.04\% & 8.7\%  \\
		0.1 & 1.57\%& 17.7\%  \\
		0.2 & 1.82\% & 26.3\%  \\
		0.3 & 2.46\% & 44.6\%  \\
		0.4 & 2\% & 57.3\%  \\
		0.5 & 2.3\% & 65.2\%  \\
		0.6 & 2.8\% & 73.2\%  \\
		0.7 & 3.07\% & 81.3\%  \\
		0.8 & 7.12\% & 89.5\%  \\
		0.9 & 11.13\% & 98.1\%  \\
		1.0 & 37.26\% & 100\%  \\
	\end{tabular}
	\caption{Percentage of misclassified data points of the whole dataset as a function the regularization parameter for the Levenberg-Marquardt method with one hidden neuron.}
	\label{tab:lmreg}
\end{table}

\subsection{Conclusion}
We have tried different numbers of hidden neurons, training methods and regularization parameters and the results are clear. It seems that only one hidden neuron is enough to deliver very good classification results. With a misclassification of only 1.5\% the standard Levenberg-Marquardt method is already a decent choice but Bayesian regularization can even perform better with an error consistently less than 1\% for a whole range of threshold values. This amounts to only a few misclassifications on the whole dataset so this is a good choice of algorithm. Finally regularization only seems to worsen the numerical results so it is not advised to use this on this dataset.
\end{document}