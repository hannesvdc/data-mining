function [TrainData,TrainTarget]=getOrderedTimeSeriesTrainData(trainset, indices)
delay = max(indices);
[TrainData, TrainTarget] = getTimeSeriesTrainData(trainset, delay);

TrainData = TrainData(indices, :);

