close all;
Y(Y == 0) = -1;

hidden = 1;
nentries = length(Y);

nplot = 5;
legends = cell(nplot, 1);
avgmis = 0.;
it = 50;
treshes = [];
for tresh = 0.0:0.1:1
for j = 1:it
    disp(j);
    net = patternnet(hidden, 'trainlm');
    net.performParam.regularization = 1e-7;
    [net, tr] = train(net, X, Y);
    
    % Check the number of misclassifications
    yhat = net(X);
    
    % Calculate ROC values
    roc = [];
    for tresh = -1:0.0001:1
        pos = yhat>=tresh;
        neg = -(yhat<tresh);
        
        TP = sum(pos == Y);
        TN = sum(neg == Y);
        FP = sum(pos == -Y);
        FN = sum(neg == -Y);
        
        sensitivity = TP/(TP+FN);
        FPR = FP/(FP+TN);
        roc = [roc, [FPR; sensitivity]];
    end
    tresh = 0.0;
    pos = yhat>=tresh;
    neg = -(yhat<tresh);
    TP = sum(pos == Y);
    TN = sum(neg == Y);
    miss = 1-(TP+TN)/nentries;
    avgmis = avgmis + miss;
    
    if j <= nplot
        legends{j} = strcat('Classification ', num2str(j));
        roc = sort(roc);
        plot(roc(1,:), roc(2,:), '-', 'LineWidth', 2); hold on;
    end
    
    %clear net;
end
    treshes = [treshes, avgmis/10];
end
xlabel('False Positive Rate');
ylabel('Sensitivity');
legend(legends);
disp(avgmis/it);