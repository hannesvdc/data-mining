close all;

autocor = xcorr(lasertrain);
[~,I] = sort(autocor, 'descend');
I = I(I > 1000);
I = I-1000;
figure(1);
plot(autocor);

% Plot the input data
figure(2);
plot(lasertrain); hold on;
plot(1001:1100, laserpred);

p = 22;
[traindata, traintarget]=getTimeSeriesTrainData(lasertrain, p);

nplot = 5;
legends = cell(nplot+1,1);
mse_avg = 0.;
it = 50;
for j = 1:it
    disp(j);
    net = fitnet(2*p, 'trainlm');
    net = configure(net, traindata, traintarget);
    net.performParam.regularization = 1e-1;
    [net, ~] = train(net, traindata, traintarget);

    % Now predict the error on the manual test set.
    input = lasertrain((end-p+1):end);
    outputs = laserpred;
    netoutputs = [];
    for i=1:(length(laserpred))
       output = net(input);
       input = [input(2:p); output];
       netoutputs = [netoutputs; output];
    end

    % Compute mse
    mse_error = (outputs-netoutputs)'*(outputs-netoutputs)/length(outputs);
    mse_avg = mse_avg + mse_error;

    if j <= nplot
        figure(3);
        t = 1:1:length(outputs);
        plot(t, netoutputs, '-', 'LineWidth', 2); hold on;
        legends{j} = strcat('Approximation ', num2str(j));
    end
    
    clear net;
    clear netoutputs;
end

t = 1:1:length(outputs);
figure(3)
plot(t, outputs, '-', 'LineWidth', 2); hold on;
legends{nplot+1} = 'Laser Prediction Set';
hold off;
xlabel('Time [s]', 'FontSize', 24);
legend(legends);
disp(mse_avg/it);