\select@language {english}
\contentsline {section}{\numberline {1}Expectation Maximization (EM) Algorithm}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Example: two Gaussians with diagonal covariance matrices}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}A second example: three Gaussian distributions}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Example 3: Diagonal covariance matrices}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Fourth example: full covariance matrices}{6}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Conclusion on EM}{7}{subsection.1.5}
\contentsline {section}{\numberline {2}Self Organizing Maps - SOM}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}First example of SOM: Manifold fitting}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}A second example: classification of flowers}{10}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Data analysis using SOM}{12}{subsection.2.3}
\contentsline {section}{\numberline {3}Application of SOM: Diabetes liver disorder}{16}{section.3}
\contentsline {subsection}{\numberline {3.1}Online versus batch learning}{18}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Different grid types}{18}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Effect of the grid size}{18}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Effect of the training epochs}{22}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Conclusion}{23}{subsection.3.5}
